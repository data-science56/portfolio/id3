This is a **Python** implementation of a **decision-tree classification model** based on the **ID3 algorithm**.  

The code is used to classify animals described in the [UCI Zoo Data Set](https://archive.ics.uci.edu/ml/datasets/Zoo).
This dataset consists of 101 rows, each of which contains the name of an animal followed by 16 binary (1 or 0) values indicating attributes defining whether or not an animal has a particular property (e.g. 'toothed', 'breathes', etc.). 
The final column is the label, which consists of an integer ranging from 1 to 7 and representing categories such as Mammal (1), Bird (2), Reptile (3), etc.  

Rather than using the existing scikit-learn decision tree model, the idea here was to create a decision tree program from scratch, in order to fully understand the inner workings of the algorithm.  

The main building blocks are:
* Calculate the *Entropy*, a measure of heterogeneity, from which we can derive the *Information Gain*, a measure of how much entropy decreases when the dataset is split according to a particular feature.
* Starting from the feature with the highest gain in the feature space, recursively build the tree as a nested dictionary.
* For every unseen data instance, recursively traverse the tree to predict the species of the animal.  

The `main.py` module reads in the data, splits the dataset into training and testing subsets, performs the aforementioned steps, and computes the percentage of correct predictions.  

Lastly, purely for convenience, we import the `sklearn.metrics` module to display the confusion matrix, which indicates actual vs. predicted species in  matrix form, where any nonzero terms outside the diagonal denote incorrect predictions.