import pandas as pd
import numpy as np

def entropy(target_col):
# Calculate entropy of dataset
    elements,counts = np.unique(target_col,return_counts = True)
# elements = array([ unique elements ]);
# counts = array([ their freqs ]);
    entropy = np.sum([(-counts[i]/np.sum(counts))*np.log2(counts[i]/np.sum(counts)) for i in range(len(elements))])
    return entropy

def show_entropy(data):
  for name in data.columns:
    print(f'Entropy for column {name}: {entropy(data[name]):.3f}')

def InfoGain(data,splitfeat,targetcol="class"):
# Calculate entropy of whole dataset
    total_entropy = entropy(data[targetcol])
# Find unique values & corresponding counts for split feature
    uniqs,freqs = np.unique(data[splitfeat],return_counts=True)
# Calculate weighted entropy
    Weighted_Entropy = np.sum([(freqs[i]/np.sum(freqs))*entropy(data.where(data[splitfeat]==uniqs[i]).dropna()[targetcol]) for i in range(len(uniqs))])
# Calculate information gain
    Information_Gain = total_entropy - Weighted_Entropy
    return Information_Gain

def show_infgain(data):
  infgn = {}
  for name in data.columns:
    if name == "class": continue
    infgn[name] = InfoGain(data,name)
# Find longest string length for right alignment
  longest = max(len(name) for name in infgn.keys())
# Sort dict in descending order of Info. Gain  
  infgn = {key: value for key, value in sorted(infgn.items(), key=lambda item: item[1],reverse=True)}
  for key, val in infgn.items():
    print(f'Information gain for column {key : >{longest}}: {val:.3f}')

def ID3(data, featspace, targetcol="class", parentnode_class=None):
# ID3 tree generator for classification
  uniqs, freqs = np.unique(data[targetcol],return_counts=True)
  if len(uniqs) <= 1:
    return uniqs[0]
  elif len(featspace)==0 or len(data)==0:
    return parentnode_class
  else:
# Identify feature with highest gain in feature space
    allgains = [InfoGain(data,feature,"class") for feature in featspace]
    bestfeatidx = np.argmax(allgains)
    best_feature = featspace[bestfeatidx]
# Initialize tree as nested dictionary
    tree = {best_feature:{}}
# Remove highest-gain feature from feature space
    featspace = [ftr for ftr in featspace if ftr != best_feature]
# Pass default value to next recursive call of function
    parentnode_class = uniqs[np.argmax(freqs)].astype('int')
#
    for uniq in np.unique(data[best_feature]).astype('int'):
      sub_data = data.where(data[best_feature] == uniq).dropna()
      tree[best_feature][uniq] = ID3(sub_data, featspace, targetcol, parentnode_class.astype('int'))
    return tree

def classify(unseen, tree, default=None):
    attribute = list(tree.keys())[0]
    if unseen[attribute] in tree[attribute].keys():
        result = tree[attribute][unseen[attribute]]
        if isinstance(result, dict): # traverse sub-tree
            return classify(unseen, result)
        else:
            return result # leaf node, no sub-tree
    else:
        return default