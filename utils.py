# See how many unique values exist in each column
def show_uniqs(data):
  for name in data.columns:
    print(name + "\t" + str(len(data[name].unique())))

# Convert 1/0 culumns to True/False
def int2bool(data):
# Find which columns take more than 2 values;
  nonbins = [name for name in data.columns if len(data[name].unique()) >2]
  for name in data.columns:
    if not name in nonbins:
      data[name] = data[name].astype(bool)

def train_test_split(data, trnfrac):
  training_data = data.sample(frac = trnfrac)
  testing_data = data.drop(training_data.index)
  return training_data,testing_data