import pandas as pd
import numpy as np
import random as rnd
from pprint import pprint
from sklearn.metrics import confusion_matrix

from ML_subs import entropy, InfoGain, ID3, classify, show_entropy, show_infgain
from utils import show_uniqs, int2bool, train_test_split

# Import dataset
dataset = pd.read_csv('data/zoo.csv', names=['animal_name','hair','feathers','eggs','milk',
'airbone','aquatic','predator','toothed','backbone',
'breathes','venomous','fins','legs','tail','domestic','catsize','class'])

# Drop animal names as not suitable to split data on
dataset=dataset.drop('animal_name',axis=1)
# Find the overall mode of the label (target) column
elements,counts = np.unique(dataset['class'],return_counts = True)
ovrl_mode = elements[np.argmax(counts)]
#
training_data, testing_data = train_test_split(dataset,0.8)
tree = ID3(training_data, training_data.columns[0:-1], "class")
#
pprint(tree)
#

testing_data['predicted'] = testing_data.apply(classify, axis=1, args=(tree,ovrl_mode) )
pprint(testing_data[['class', 'predicted']])
print('Prediction accuracy: ',(np.sum(testing_data['predicted'] == testing_data['class'])/len(testing_data))*100,'%')
print(confusion_matrix(testing_data['predicted'], testing_data['class'], labels=None))